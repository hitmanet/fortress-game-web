const BranchController  = require('../controllers/BranchController')

module.exports = (app) => {
    app.get('/api/branches/getAll',BranchController.getAll);
    app.get('/api/branches/getBranch',BranchController.getBranch);
    app.get('/api/branches/createBranch',BranchController.createBranch);
    app.post('/api/branches/compoundTaskToBranch',BranchController.compoundTaskBranch)
}