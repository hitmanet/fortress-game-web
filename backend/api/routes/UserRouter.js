const UserController  = require('../controllers/UserController')

module.exports = (app) => {
    app.get('/api/users/getUser', UserController.getUser);
    app.get('/api/users/getInProcessTasks',UserController.getInProcessTasks)
}