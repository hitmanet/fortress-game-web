const TaskController = require('../controllers/TaskController')

module.exports = (app) => {
    app.post('/api/tasks/updateState',TaskController.updateState)
    app.post('/api/tasks/createTask',TaskController.createTask)
}