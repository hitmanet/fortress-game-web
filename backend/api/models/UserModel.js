const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
        default: this.username
    },
    icon:{
        type: String,
        default: 'https://psv4.userapi.com/c848132/u220662947/docs/d15/a792124da148/model_person.png?extra=avUKYHUpvYFPZjZHcB6qHZ9y1KlDIi6LlaoU11RUFjKJ0JCvs25Ftm5x1v4u6-8oFMu_H3jHQF0qOsxpQVtbDJq1Ix71GIYmJOmjtiebrdJcuDHIBJLF-ejOCLJwWakEMInF0uQRzE7Fk_vKXT8OKHFp'
    },
    branches:[{
        branch: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Branch',
            require: true,
            unique: true
        },
        tasks: [{
            task: {
                type: Schema.Types.ObjectId,
                ref: 'Task'
            },
            state: {
                type: String,
                enum: [
                    'not_started',
                    'in_process',
                    'completed',
                    'expired'
                ],
                default: 'not_started'
            }
        }],
        exp: {
            type: Number,
            default: 1
        }
    }]
})


mongoose.model('User',UserSchema)