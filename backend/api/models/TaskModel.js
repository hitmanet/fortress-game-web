    const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TaskSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    description: String,
    deadline: {
        type: Date,
        required: true,
        default: new Date().getTime() + 86400 
    },
    icon: {
        type:String,
        default: 'https://st3.depositphotos.com/10654668/15053/i/450/depositphotos_150535932-stock-photo-one-yellow-tulip.jpg'
    },
    award: {
        type: Number,
        default: 0
    },
    fine: {
        type: Number,
        default: 0
    },
    exp: {
        type: Number,
        default: 10
    },
    loc: String
    // loc: {
    //     type: { type: String },
    //     coordinates: [Number]
    // }
})

// TaskSchema.index({ "loc": "2dsphere" })

mongoose.model('Task',TaskSchema)