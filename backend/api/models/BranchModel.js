    const mongoose = require('mongoose')

const Schema = mongoose.Schema


const BranchSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    description: String,
    tasks:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task'
    }],
    level: {
        type: Number,
        default: 1
    },
    icon:{
        type: String,
        default: 'https://pp.userapi.com/c846121/v846121569/131cb6/zfTpKPAbKpM.jpg'
    }
})


mongoose.model('Branch',BranchSchema)