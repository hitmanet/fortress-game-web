const mongoose = require('mongoose')
const Task = mongoose.model('Task')
const User = mongoose.model('User')
const Branch = mongoose.model('Branch')

// task_id branch_id new_state
module.exports.updateState   = (req,res) => {
    User.findOne({})
        .populate('branches.branch')
        .populate('branches.tasks.task')
    .exec((err,d) => {


    let branch_i = null,
        task_id = null;
       let a = d.branches.find((v,i) => {
            if (v.branch._id == req.body.branch_id){
                branch_i = i;
                return true;
            }else return false;
       });

    if(a){
        let b = d.branches[branch_i].tasks.find((v,i) => {
            if(v.task._id == req.body.task_id){
                task_id = i;
                return true
            }else return false;
        })

        if (b){
            d.branches[branch_i].tasks[task_id].state = req.body.new_state;
            if(req.body.new_state == 'completed' || req.body.new_state == 'expired') {
                d.exp = d.exp + d.branches[branch_i].tasks[task_id].task[req.body.new_state == 'completed'? 'award': 'fine'];
                console.log('exp: ',d.exp  )
            }
            d.save(() => res.sendStatus(200))
        }else{
            res.sendStatus(405)
        }
    }else{
        res.sendStatus(404)
    }

    })
}

module.exports.createTask = (req,res) => {
    let b = req.body;
    let newTask = new Task({
        name: b.name,
        description: b.description,
        loc: b.loc  
    }).save(() => res.sendStatus(200))
}