const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports.getUser = (req,res) => {
    User.findOne({}).populate('branches.branch').populate('branches.tasks.task').exec((err,d) => {
        if (!err) {
            d.branches.map((_branch,b_i) => {
                _branch.tasks.map((_task,t_i) => {
                    if(_task.task.deadline < new Date().getTime()){
                        _task.state = 'expired'
                        // console.log(Date(_task.task.deadline).getTime(), '       ', new Date().getTime())
                    }
                })
            })
            d.save(() => res.json(d))  
        }else res.sendStatus(404)
    })
}

module.exports.getInProcessTasks = (req,res) => {
    User.findOne({},{branches: true}).populate('branches.branch',{_id: req.query.branch_id}).populate('branches.tasks.task').exec((err,d) => {
       let in_process = []
        d.branches[0].tasks.map((e) => {
            if (e.state == 'in_process') in_process.push(e.task);
       })
        res.json(in_process)
    })
}
