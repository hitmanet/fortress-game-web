const mongoose = require('mongoose')
const Branch = mongoose.model('Branch')
const Task = mongoose.model('Task')

module.exports.getAll = (req,res) => {
    Branch.find({}).populate('tasks').exec((err,d) => {
        res.json(d)
    })
}
module.exports.getBranch = (req,res) => {
    if (req.query._id){
        Branch.findById(req.query._id).populate('tasks').exec((err,branch) => {
            res.json(branch)
        })
    }else  res.sendStatus(404)
}

module.exports.createBranch = (req,res) => {
    let b = req.body;

    let newBranch = new Branch({
        name: b.name,
        description: b.description,
    }).save(() => res.sendStatus(200))
}

module.exports.compoundTaskBranch = (req,res) => {
    Branch.findById(req.body.branch_id,(err,br) => {
        br.tasks.push(req.body.task_id)
        br.save(() => sendStatus(200))
    })
}