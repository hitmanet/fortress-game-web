const express = require('express')
const app = express()

const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

bodyParser = require('body-parser');

// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','POST, GET, OPTIONS');
    next()
})

require('dotenv').config()

mongoose.connect("mongodb://localhost:27017/fortress", { useNewUrlParser: true });

mongoose.set('useFindAndModify', false);


// Authorization middleware
// app.use((req,res,next) => {
//   const authHeader = req.headers['Authorization'];
//   if (authHeader){
//     const splice = authHeader.split(' ');
//     jwt.verify(splice[1],process.env.JWT_SECRET, (err,decode) => {
//       if (!err) {
//         req.user = decode
//       }else req.user = null  
//     })
//   } else req.user = null;
//   next();
// })

// models import
const Task = require('./api/models/TaskModel');
const Branch = require('./api/models/BranchModel')
const User = require('./api/models/UserModel')

// controller import
const BranchController  = require('./api/controllers/BranchController')

// routes import
const UserRouter = require('./api/routes/UserRouter')(app)
const BranchRouter = require('./api/routes/BranchRouter')(app)
const TaskRouter = require('./api/routes/TaskRouter')(app)



app.listen(3000, () =>  console.log('server localhost:3000'))
