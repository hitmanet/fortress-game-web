import Vue from 'vue'
import Router from 'vue-router'
import Branches from '../components/Branches/index'
import PrivateOffice from '../components/PrivateOffice/index'
import BaseInfo from '../components/baseInfo.vue'
import Map from '../components/map.vue'
import TaskMap from '../components/TaskMap/index'
import TabPage from '../components/TabPage/index'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/branches',
            name: 'Branches',
            component: Branches
        },
        {
            path: '/profile',
            name: 'PrivateOffice',
            component: PrivateOffice,
        },
        {
            path: '/baseInfo',
            name: 'BaseInfo',
            component: BaseInfo,
        },
        {
            path: '/:branch_id/tasks',
            name: 'TabPage',
            component: TabPage
        },
        {
            path: '/:branch_id/tasks',
            name: 'TaskMap',
            component: TaskMap
        },
        {
            path: '/map',
            name: 'Map',
            component: Map
        }
    ]
})
