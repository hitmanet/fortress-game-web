import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        currentUser: null,
        branches: [],
        tasks: [],
        inProcessTasks: []
    },
    actions: {
        setUser({ commit }, user) {
            commit('SET_USER', user)
        },
        setBranches({ commit }, branches) {
            commit('SET_BRANCHES', branches)
        },
        setTasks({ commit }, tasks) {
            commit('SET_TASKS', tasks)
        },
        setInProcessTasks({ commit }, tasks){
            commit('SET_IN_PROCESS_TASKS', tasks)
        }
    },
    mutations: {
        SET_USER(state, user) {
            state.currentUser = user
        },
        SET_BRANCHES(state, branches) {
            state.branches = branches
        },
        SET_TASKS(state, tasks) {
            state.tasks = tasks
        },
        SET_IN_PROCESS_TASKS(state, tasks){
            state.inProcessTasks = tasks
        }
    },
    getters: {
        user(state) {
            return state.currentUser
        },
        branches(state) {
            return state.branches
        },
        tasks(state) {
            return state.tasks
        },
        inProcessTasks(state){
            return state.inProcessTasks
        }
    }
})