import axios from 'axios'

const host = 'http://192.168.161.53:3000'

const callback = (response) => response.data

export default async(url, params = {}) => {
    const res = await axios.get(`${host}/api/${url}`, { params })
    return await callback(res)
}

