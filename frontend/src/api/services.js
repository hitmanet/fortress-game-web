import apiHelper, { axiosPost } from './index'

export default {
    getBranchInfo: (id) => apiHelper('branches/getBranch', { _id: id }),
    getUser: () => apiHelper('users/getUser'),
    setTaskState: (body) => apiHelper('tasks/updateState', {
        new_state: body.new_state,
        branch_id: body.branch_id,
        task_id: body.task_id
    }),
    getInProcessTask: (branchId) => apiHelper('users/getInProcessTasks', { branch_id: branchId })

}