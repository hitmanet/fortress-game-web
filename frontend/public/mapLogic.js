ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("map", {

            center: [47.22989933, 39.74528938],
            // от 0 (весь мир) до 19.
            zoom: 15
        }),
        taskPoint = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [47.22989933, 39.74528938]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: 'Новое задание',
                hintContent: '*описание задания*',
                // preset: 'islands#redIcon'
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#redStretchyIcon',
            // Метку можно перемещать.
            // draggable: true
        })

        taskPoint.events.add('click', function () {
            alert('О, событие!');
            router.push({ path: '/profile' })
        });


          myMap.geoObjects.add(taskPoint)

    };

// Размещение геообъекта на карте.

  // var myPlacemark = new ymaps.Placemark([47.22989933, 39.74528938]);
//   var myPlacemark = new ymaps.GeoObject({
//     geometry: {
//         type: "Point",
//         coordinates: [47.22989933, 39.74528938]
//     }
// });
